import cv2
import sys
from shapely.geometry import Polygon, box
import time


def bb_intersection_over_union(poly_1, poly_2, test = False):
    iou = poly_1.intersection(poly_2).area / poly_1.union(poly_2).area
    return iou


def track_faces(curr_face, previous_frames):
    """Check iou and only return True if iou better than 0.75."""
    if len(previous_frames.keys()) < 1:
        return True
    hits = 0
    print(f"Current face : {curr_face}")
    print(f"Number of previous frames: {len(previous_frames.items())}")
    for _, prev_faces in previous_frames.items():
        for (xp, yp, wp, hp) in prev_faces:
            xp1 = xp + wp
            yp1 = yp + hp
            box_prev = box(xp, yp, xp1, yp1)
            print(f"Previous face : {box_prev}")
            print(bb_intersection_over_union(curr_face, box_prev))
            if bb_intersection_over_union(curr_face, box_prev) >= 0.75:
                hits += 1
                print(f"Hits: {hits}")
            else:
                bb_intersection_over_union(curr_face, box_prev, True)
    if hits > 3:
        return True
    return False


face_cascade = cv2.CascadeClassifier('data/' + 'haarcascade_frontalface_default.xml')
eye_cascade = cv2.CascadeClassifier('data/' + 'haarcascade_eye.xml')

video_capture = cv2.VideoCapture(0)
frameno = 0
faces_per_frame = {}

while True:
    # Capture frame-by-frame
    ret, frame = video_capture.read()
    if frameno == 5:
        frameno = 0

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces = face_cascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv2.CASCADE_SCALE_IMAGE
    )
    
    # Draw a rectangle around the faces
    
    for (x, y, w, h) in faces:
        x1 = x+w
        y1 = y+h
        poly_1 = box(x, y, x1, y1)
        if not track_faces(poly_1, faces_per_frame):
            continue
        img = cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = img[y:y+h, x:x+w]
        eyes = eye_cascade.detectMultiScale(roi_gray)
        # for (ex,ey,ew,eh) in eyes:
        #     cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)

    faces_per_frame[frameno] = faces
    frameno += 1
    # Display the resulting frame
    cv2.imshow('Video', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything is done, release the capture
video_capture.release()
cv2.destroyAllWindows()